#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, json, request,redirect,session,jsonify,flash,url_for
from flask.ext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash

mysql = MySQL()
app = Flask(__name__)
app.secret_key = 'why would I tell you my secret key?'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'tes'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/Registrarse')
def Registrarse():
    return render_template('signup.html')

@app.route('/signUp', methods=['POST','GET'])
def signUp():
    try:
        _name = request.form['inputName']
        _email = request.form['inputEmail']
        _password = request.form['inputPassword']

        # validate the received values
        if _name and _email and _password:

            # All Good, let's call MySQL

            conn = mysql.connect()
            cursor = conn.cursor()
            _hashed_password = generate_password_hash(_password)
            cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return render_template('index.html')
            else:
                return render_template('error.html',error = 'Error no se pudo registrar')
        else:
            return render_template('error.html',error = 'Todos los campos son requeridos')

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close()
            conn.close()

@app.route('/Ingresar')
def Ingresar():
	return render_template('signin.html')

@app.route('/validateLogin',methods=['POST','GET'])
def validateLogin():
    try:
        _username = request.form['inputEmail']
        _password = request.form['inputPassword']



        # connect to mysql

        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('sp_validateLogin',(_username,))
        data = cursor.fetchall()

        if len(data) > 0:
            if check_password_hash(str(data[0][3]),_password):
                session['user'] = data[0][0]

                return redirect('/userHome')
            else:
            	flash('El email o la password estan mal escritos.')
                return redirect('/Ingresar')
        else:
        	flash('El email o la password estan mal escritos.')
        	return redirect('/Ingresar')
        cursor.close()
        con.close()
    except Exception as e:
        return render_template('error.html',error = str(e))
    finally:
            print('happy destruction')

@app.route('/userHome')
def userHome():
    if session.get('user'):
        return render_template('userHome.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/logout')
def logout():
    session.pop('user',None)
    return redirect('/')

@app.route('/Registro')
def Registro():
    return render_template('addWish.html')

@app.route('/addAnimal',methods=['POST','GET'])
def addAnimal():
    try:
        if session.get('user'):
            _caravana=request.form['caravana']
            _nombre=request.form['nombre']
            _fechana=request.form['fechana']
            _icc=request.form['icc']
            _nopartos=request.form['nopartos']
            _rodeo=request.form['rodeo']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_registro',(_caravana,_nombre,_fechana,_icc,_nopartos,_rodeo))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
          
                return redirect('/userHome')
            else:
            	er=json.dumps(data)
            	if er =='[["existe"]]':
            		flash('La caravana especificada ya existe')
            		return redirect(url_for('Registro'))

        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = str(e))
    #finally:
     #   cursor.close()
      #  conn.close()

@app.route('/getAltas')
def getAltas():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_altas')
            altas = cursor.fetchall()

            all_altas = []
            for item in altas:
                alta_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_altas.append(alta_dict)
            return json.dumps(all_altas)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getMedias')
def getMedias():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_medias')
            medias = cursor.fetchall()

            all_medias = []
            for item in medias:
                media_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_medias.append(media_dict)
            return json.dumps(all_medias)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getBajas')
def getBajas():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_bajas')
            bajas = cursor.fetchall()

            all_bajas = []
            for item in bajas:
                baja_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_bajas.append(baja_dict)
            return json.dumps(all_bajas)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getEnfermeria')
def getEnfermeria():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_enfermeria')
            enfermeria = cursor.fetchall()

            all_enfermeria = []
            for item in enfermeria:
                enfermeria_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_enfermeria.append(enfermeria_dict)
            return json.dumps(all_enfermeria)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getSecas')
def getSecas():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_secas')
            secas = cursor.fetchall()

            all_secas = []
            for item in secas:
                secas_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_secas.append(secas_dict)
            return json.dumps(all_secas)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getEliminadas')
def getEliminadas():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_eliminada')
            eliminadas = cursor.fetchall()

            all_eliminadas = []
            for item in eliminadas:
                eliminadas_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_eliminadas.append(eliminadas_dict)
            return json.dumps(all_eliminadas)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getAnimalById',methods=['POST'])
def getWishById():
    try:
        if session.get('user'):

            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_getAnimalById',(_id,))
            result = cursor.fetchall()

            item = []
            item.append({'Caravana':result[0][0],'Nombre':result[0][3],'Fecha':str(result[0][4]),'Icc':result[0][5],'No_partos':result[0][6],'Rodeo':result[0][1]})

            return json.dumps(item)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = str(e))

@app.route('/updateAnimal', methods=['POST'])
def updateWish():
    try:
        if session.get('user'):
            _caravana=request.form['id']
            _nombre=request.form['nombre']
            _fechana=request.form['fechana']
            _icc=request.form['icc']
            _nopartos=request.form['nopartos']
            _rodeo=request.form['rodeo']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_updateAnimal',(_caravana,_nombre,_fechana,_icc,_nopartos,_rodeo))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'ERROR'})
    except Exception as e:
        return json.dumps({'status':'Unauthorized access'})
    finally:
        cursor.close()
        conn.close()

@app.route('/bajaAnimal',methods=['POST'])
def deleteWish():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_bajaAnimal',(_id,))
            result = cursor.fetchall()

            if len(result) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'An Error occured'})
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return json.dumps({'status':str(e)})
    finally:
        cursor.close()
        conn.close()

@app.route('/eliminarAnimal',methods=['POST'])
def eliminarAnimal():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_eliminarAnimal',(_id,))
            result = cursor.fetchall()

            if len(result) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'An Error occured'})
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return json.dumps({'status':str(e)})
    finally:
        cursor.close()
        conn.close()

@app.route('/cambiaRodeo',methods=['POST'])
def cambiaRodeo():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_cambiaRodeo',(_id,))
            result = cursor.fetchall()

            if len(result) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'An Error occured'})
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return json.dumps({'status':str(e)})
    finally:
        cursor.close()
        conn.close()

@app.route('/reproduccion')
def reproduccion():
    if session.get('user'):
        return render_template('reproduccion.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/getReproduccion')
def getReproduccion():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_reproduccion')
            reproduccion = cursor.fetchall()


            all_reproduccion = []
            for item in reproduccion:
                reproduccion_dict = {
                        'Caravana': item[0],
                        'Nombre': item[1],
                        'No_partos': item[2],
                        'Fecha_inse': str(item[3]),
                        'No_inse': item[4],
                        'Rodeo': item[5]}
                all_reproduccion.append(reproduccion_dict)
            return json.dumps(all_reproduccion)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getPartos')
def getPartos():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_partos')
            partos = cursor.fetchall()

            all_partos = []
            for item in partos:
                partos_dict = {
                        'Caravana': item[0],
                        'Nombre': item[1],
                        'No_partos': item[2],
                        'Fecha_pre': str(item[3]),
                        'Rodeo': item[4]}
                all_partos.append(partos_dict)
            return json.dumps(all_partos)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getLactancia')
def getLactancia():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_lactancia')
            lactancia = cursor.fetchall()

            all_lactancia = []
            for item in lactancia:
                lactancia_dict = {
                        'Caravana': item[0],
                        'Nombre': item[1],
                        'No_partos': item[2],
                        'Fecha_par': str(item[3]),
                        'Rodeo': item[4]}
                all_lactancia.append(lactancia_dict)
            return json.dumps(all_lactancia)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/getInse',methods=['POST'])
def getInse():
    try:
        if session.get('user'):

            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_GetInseId',(_id,))
            result = cursor.fetchall()

            item = []
            item.append({'Caravana':result[0][0],'Fecha_inse':str(result[0][1]),'No_inse':result[0][2]})

            return json.dumps(item)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = str(e))

@app.route('/updateInse', methods=['POST'])
def updateInse():
    try:
        if session.get('user'):
            _caravana=request.form['id']
            _fechainse=request.form['fechainse']
            _noinse=request.form['noinse']

            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_updateInse',(_caravana,_fechainse,_noinse))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'ERROR'})
    except Exception as e:
        return json.dumps({'status':'Unauthorized access'})
    finally:
            cursor.close()
            conn.close()

@app.route('/cambiaRep',methods=['POST'])
def cambiaRep():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_cambiaRep',(_id,))
            result = cursor.fetchall()

            if len(result) is 0:
                conn.commit()
                return json.dumps({'status':'OK'})
            else:
                return json.dumps({'status':'An Error occured'})
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return json.dumps({'status':str(e)})
    finally:
        cursor.close()
        conn.close()

@app.route('/buscarAnimal', methods=['POST'])
def buscarAnimal():
    try:
        if session.get('user'):
            _user = session.get('user')
            _nombre=request.form['buscar']

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_buscarAnimal',(_nombre,))
            buscar = cursor.fetchall()

            all_buscar = []
            for item in buscar:
                buscar_dict = {
                        'Caravana': item[0],
                        'Nombre': item[3],
                        'Fecha': str(item[4]),
                        'Icc': item[5],
                        'No_partos': item[6],
                        'Rodeo': item[1]}
                all_buscar.append(buscar_dict)
                
            print(all_buscar)
            return json.dumps(all_buscar)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error = str(e))

@app.route('/Resumen')
def Resumen():
    if session.get('user'):

        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('sp_grafica')
        count = cursor.fetchall()

        alta=count[0][0]
        media=count[0][1]
        baja=count[0][2]
        enfermeria=count[0][3]
        secas=count[0][4]
        eliminadas=count[0][5]
        cursor.close()

        cursor = con.cursor()
        cursor.callproc('sp_grafica2')
        result = cursor.fetchall()

        rep=result[0][0]
        par=result[0][1]
        lac=result[0][2]

        cursor.close()
        con.close()

        return render_template('resumen.html',alta=alta,media=media,baja=baja,enfermeria=enfermeria,secas=secas,eliminadas=eliminadas,rep=rep,par=par,lac=lac)
    else:
        return render_template('error.html',error = 'Unauthorized Access')

if __name__ == '__main__':
    app.run(debug=True)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
