DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_altas`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Alta';
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_medias`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Media';
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_bajas`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Baja';
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_enfermeria`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Enfermeria';
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_secas`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Secas';
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminada`()
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and rodeo='Eliminada';
END$$
DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `sp_registro`;

DELIMITER $$
CREATE PROCEDURE `sp_registro`(
    IN p_caravana varchar(10),
    IN p_nombre varchar(45),
    IN p_fechana date,
    IN p_icc varchar(45),
    IN p_nopartos int(5),
    IN p_rodeo varchar(45)
)
BEGIN
if (select EXISTS (SELECT id_animal from animales where id_animal=p_caravana)) then
SELECT "existe";
else 
    INSERT INTO animales (id_animal, rodeo) VALUES (p_caravana, p_rodeo);
    INSERT INTO info_animal (id_animal, nombre, fecha_na, icc, no_partos) VALUES (p_caravana, p_nombre, p_fechana, p_icc, p_nopartos);
end if;
END$$

DELIMITER ;
;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getAnimalById`(
IN p_animal_id varchar(20)
)
BEGIN
     SELECT * FROM animales, info_animal where info_animal.id_animal=p_animal_id and animales.id_animal=p_animal_id;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateAnimal`(
IN p_caravana varchar(45),
IN p_nombre varchar(45),
IN p_fechana date,
IN p_icc varchar(45),
IN p_nopartos int(5),
In p_rodeo varchar(45)
)
BEGIN
UPDATE animales SET id_animal=p_caravana, rodeo=p_rodeo WHERE id_animal=p_caravana;
UPDATE info_animal SET id_animal=p_caravana, nombre=p_nombre, fecha_na=p_fechana, icc=p_icc, no_partos=p_nopartos WHERE id_animal=p_caravana;
END$$
DELIMITER ;

DELIMITER $$
USE `tes`$$
CREATE PROCEDURE `sp_bajaAnimal` (
IN p_animal_id bigint
)
BEGIN
UPDATE animales SET rodeo='Eliminada' WHERE id_animal=p_animal_id;
END$$

DELIMITER ;

DELIMITER $$
USE `tes`$$
CREATE PROCEDURE `sp_eliminarAnimal` (
IN p_animal_id bigint
)
BEGIN
    DELETE info_animal,animales FROM info_animal JOIN animales on info_animal.id_animal=animales.id_animal where animales.id_animal=p_animal_id;
END$$

DELIMITER ;


DELIMITER $$
USE `tes`$$
CREATE PROCEDURE `sp_cambiaRodeo` (
IN p_animal_id bigint
)
BEGIN
SELECT rodeo into @rodeo from animales where id_animal=p_animal_id;
if @rodeo = 'Alta' then
  UPDATE animales SET rodeo='Media' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Media' then
  UPDATE animales SET rodeo='Baja' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Baja' then
    UPDATE animales SET rodeo='Enfermeria' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Enfermeria' then
    UPDATE animales SET rodeo='Secas' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Secas' then
    UPDATE animales SET rodeo='Alta' WHERE id_animal=p_animal_id;
end if;
END$$

DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_reproduccion`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reproduccion`()
BEGIN
SELECT animales.id_animal,nombre,no_partos,fecha_inse,no_inse,animales.rodeo FROM animales INNER JOIN info_animal on animales.id_animal=info_animal.id_animal
INNER JOIN inseminacion on animales.id_animal=inseminacion.id_animal;
END$$
DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_partos`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_partos`()
BEGIN
SELECT animales.id_animal,nombre,no_partos,fecha_pre,animales.rodeo FROM animales INNER JOIN info_animal on animales.id_animal=info_animal.id_animal
INNER JOIN partos on animales.id_animal=partos.id_animal;
END$$
DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_lactancia`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lactancia`()
BEGIN
SELECT animales.id_animal,nombre,no_partos,Fecha_par,animales.rodeo FROM animales INNER JOIN info_animal on animales.id_animal=info_animal.id_animal
INNER JOIN lactancia on animales.id_animal=lactancia.id_animal;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_GetInseId`(
IN p_animal_id varchar(20)
)
BEGIN
    SELECT * FROM inseminacion where id_animal=p_animal_id;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateInse`(
IN p_caravana varchar(45),
IN p_fechainse date,
IN p_noinse bigint

)
BEGIN
    UPDATE inseminacion SET fecha_inse=p_fechainse, no_inse=p_noinse WHERE id_animal=p_caravana;
END$$
DELIMITER ;

DELIMITER $$
USE `tes`$$
CREATE PROCEDURE `sp_cambiaRep` (
IN p_animal_id bigint
)
BEGIN
SELECT rodeo into @rodeo from animales where id_animal=p_animal_id;
if @rodeo = 'Alta' then
  UPDATE animales SET rodeo='Enfermeria' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Media' then
  UPDATE animales SET rodeo='Enfermeria' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Baja' then
    UPDATE animales SET rodeo='Enfermeria' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Enfermeria' then
    UPDATE animales SET rodeo='Secas' WHERE id_animal=p_animal_id;
elseif @rodeo = 'Secas' then
    UPDATE animales SET rodeo='Alta' WHERE id_animal=p_animal_id;
end if;
END$$

DELIMITER ;


USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_buscarAnimal`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_buscarAnimal`(
  IN p_nombre varchar(45)
)
BEGIN
    SELECT * FROM animales, info_animal where animales.id_animal=info_animal.id_animal and nombre like concat("%",p_nombre,"%");
END$$
DELIMITER ;


USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_grafica`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_grafica`()
BEGIN
    SELECT count(*) into @p_alta FROM animales where rodeo='Alta';
    SELECT count(*) into @p_media FROM animales where rodeo='Media';
    SELECT count(*) into @p_baja FROM animales where rodeo='Baja';
    SELECT count(*) into @p_enfermeria FROM animales where rodeo='Secas';
    SELECT count(*) into @p_secas FROM animales where rodeo='Enfermeria';
    SELECT count(*) into @p_eliminadas FROM animales where rodeo='Eliminada';
    SELECT @p_alta,@p_media,@p_baja,@p_enfermeria,@p_secas,@p_eliminadas;
END$$
DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_grafica2`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_grafica2`()
BEGIN
    SELECT count(*) into @rep FROM inseminacion;
    SELECT count(*) into @par FROM partos;
    SELECT count(*) into @lac FROM lactancia;
    SELECT @rep,@par,@lac;
END$$
DELIMITER ;

USE `tes`;
DROP procedure IF EXISTS `tes`.`sp_alertInse`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alertInse`()
BEGIN

SELECT nombre,no_inse FROM animales INNER JOIN info_animal on animales.id_animal=info_animal.id_animal
INNER JOIN inseminacion on animales.id_animal=inseminacion.id_animal;

END$$
DELIMITER ;
